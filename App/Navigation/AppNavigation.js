//Library navigation
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

//Pages
import Splash from "../Pages/Splash";
import Login from '../Pages/Login'
import Register from '../Pages/Register'
import HomeNavigation from './HomeNavigation';
import Home from '../Pages/HomePages/Home'
import Products from '../Pages/HomePages/Products'

const AppNavigator = createStackNavigator({
    Splash: { screen: Splash, navigationOptions: { headerShown: false }},
    Login: { screen: Login, navigationOptions: { headerShown: false }},
    Register: { screen: Register, navigationOptions: { headerShown: false }},
    HomeNavigation: { screen: HomeNavigation, navigationOptions: { headerShown: false }},
    Home: { screen: Home, navigationOptions: { headerShown: false }},
    Products: { screen: Products, navigationOptions: { headerShown: false }},
}, {
    initialRouteName: 'Splash'
})

export default createAppContainer(AppNavigator);