import AsyncStorage from '@react-native-community/async-storage';
import React, { Component } from 'react'
import { BackHandler, Text, View, TouchableOpacity, StyleSheet, Alert, Image } from 'react-native'
import { NavigationEvents } from 'react-navigation'

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nama: '',
            photo: ''
        }
    }

    backPressed = () => {
		Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            }, ], {
                cancelable: false
            }
        )
		return true;
    };

    componentDidMount() {
        this.checkDataLogin()
		BackHandler.addEventListener('hardwareBackPress', this.backPressed);
	}
	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    async checkDataLogin() {
        let userLogin = JSON.parse(await AsyncStorage.getItem("userLogin"))
        let dataUser = JSON.parse(await AsyncStorage.getItem('dataUser'))
        console.log(JSON.stringify(userLogin) + ' => ' + JSON.stringify(dataUser))

        let indexUser = dataUser.findIndex((val) => val.nama == userLogin.name)
        
        this.setState({
            nama: dataUser[indexUser].nama,
            photo: dataUser[indexUser].photoUri
        })
    }

    render() {
        return(
            <View style={styles.container}>
                <NavigationEvents            
                    onDidFocus={() => this.checkDataLogin()}
                />
                <Image source={{ uri : this.state.photo}} style={styles.image} />
                <Text style={styles.textName}>
                    {this.state.nama}
                </Text>

                <TouchableOpacity
                    activeOpacity = {.7}
                    style = {styles.button}
                    onPress={() => this.backPressed()}
                >
                    <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>
                        Logout
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: 200,
        height: 200,
        borderRadius: 200 / 2,
        overflow: "hidden",
        borderWidth: 3,
        borderColor: "#D27025"
    },
    textName: {
        marginTop: 20,
        fontSize: 30,
        fontWeight: 'bold'
    },
    button: {
        marginTop: 20,
        width: '70%',
        padding: 10,
        backgroundColor: '#D27025', 
        borderWidth: 1, 
        borderColor: 'white',
        borderRadius: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    }
})