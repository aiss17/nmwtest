import AsyncStorage from '@react-native-community/async-storage';
import React, { Component } from 'react'
import { BackHandler, Text, View, TouchableOpacity, Alert, StyleSheet, FlatList, Image, TextInput } from 'react-native'
import { NavigationEvents } from 'react-navigation'
import axios from 'axios'
import Images from '../../Assets/Images';
import Icon from 'react-native-vector-icons/AntDesign'
import Modal from 'react-native-modal';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataProducts: [],
            backUpListProduct: [],
            visibleEdit: false,
            visibleAdd: false,
            id: '',
            editName: '',
            editDetail: ''
        }
    }

    backPressed = () => {
		Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            }, ], {
                cancelable: false
            }
        )
		return true;
    };

    deletePressed = (id) => {
		Alert.alert(
            'Delete Product',
            'Are you sure want to delete this product?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'Delete',
                onPress: () => this.deleteDataProduct(id)
            }, ], {
                cancelable: false
            }
        )
		return true;
    };

    componentDidMount() {
        this.getDataProducts()
		BackHandler.addEventListener('hardwareBackPress', this.backPressed);
	}
	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    async getDataProducts() {
        let userLogin = JSON.parse(await AsyncStorage.getItem("userLogin"))
        let backUpListProduct = JSON.parse(await AsyncStorage.getItem("listPoducts"))
        console.log(JSON.stringify(userLogin))

        const options = {
            headers: {
                Accept : 'application/json',
                'Content-Type' : 'application/json',
                'Authorization': 'Bearer ' + userLogin.token
            }
        };

        axios.get(`https://api.nmwclinic.co.id/products`, options)
        .then((res) => {
            console.log(res.data.data)
            if(res.data.status) {
                this.setState({
                    dataProducts: res.data.data
                })
                AsyncStorage.setItem("listPoducts", JSON.stringify(res.data.data))
            } else {
                this.setState(
                    backUpListProduct
                )
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    async addDataProduct() {
        let userLogin = JSON.parse(await AsyncStorage.getItem("userLogin"))

        var postData = JSON.stringify({
            name: this.state.editName,
            detail: this.state.editDetail
        });
        const options = {
            headers: {
                Accept : 'application/json',
                'Content-Type' : 'application/json',
                'Authorization': 'Bearer ' + userLogin.token
            }
        };

        axios.post(`https://api.nmwclinic.co.id/products`, postData, options)
        .then((res) => {
            console.log(res.data.data)
            if(res.data.status) {
                this.getDataProducts()
                this.setState({
                    visibleAdd: false
                })
            } 
        }).catch((err) => {
            console.log(err)
        })
    }

    async putDataProduct(id, nama, detail) {
        let userLogin = JSON.parse(await AsyncStorage.getItem("userLogin"))
        console.log(id + ' ' + nama + ' ' + detail)

        const data =  { name: nama, detail: detail }
        const options = {
            headers: {
                Accept : 'application/json',
                'Content-Type' : 'application/json',
                'Authorization': 'Bearer ' + userLogin.token
            }
        };

        axios.put(`https://api.nmwclinic.co.id/products/${id}`, data, options)
        .then((res) => {
            console.log(res.data.data)
            if(res.data.status) {
                this.getDataProducts()
                this.setState({
                    visibleEdit: false
                })
            } 
        }).catch((err) => {
            console.log(err)
        })
    }

    async deleteDataProduct(id) {
        let userLogin = JSON.parse(await AsyncStorage.getItem("userLogin"))
        
        const options = {
            headers: {
                Accept : 'application/json',
                'Content-Type' : 'application/json',
                'Authorization': 'Bearer ' + userLogin.token
            }
        };

        axios.delete(`https://api.nmwclinic.co.id/products/${id}`, options)
        .then((res) => {
            console.log(res.data.data)
            if(res.data.status) {
                this.getDataProducts()
            } 
        }).catch((err) => {
            console.log(err)
        })
    }

    render() {
        return(
            <View style={styles.container}>
                <NavigationEvents            
                    onDidFocus={() => this.getDataProducts()}
                />

                <Modal isVisible={this.state.visibleEdit} 
                    style={styles.modal}
                    onBackButtonPress={() => this.setState({ visibleEdit: false })}
                >
                    <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'flex-start' }}>
                            <Text style={{ fontSize: 30, fontWeight: 'bold', marginRight: 5}}>Edit</Text>
                            <Icon 
                                name='edit' 
                                size={20} 
                                style={{justifyContent: 'center', alignSelf: 'center'}}
                            />
                        </View>

                        <View style={[styles.frame, { marginTop: 20 }]}>
                            <Text style={styles.label}>
                                Name
                            </Text>
                            <TextInput
                                ref={(input) => { this.editName = input; }}
                                keyboardType={'default'}
                                onSubmitEditing={() => { this.editDetail.focus(); }}
                                editable={true}
                                style={styles.textInput}
                                multiline={false}
                                returnKeyType={'next'}
                                value={this.state.editName}
                                onChangeText={(text) => this.setState({ editName: text })}
                                placeholder={"Enter your name..."}
                            />
                        </View>

                        <View style={styles.frame}>
                            <Text style={styles.label}>
                                Detail
                            </Text>
                            <TextInput
                                ref={(input) => { this.editDetail = input; }}
                                keyboardType={'default'}
                                editable={true}
                                style={styles.textInput}
                                multiline={false}
                                returnKeyType={'done'}
                                value={this.state.editDetail}
                                onChangeText={(text) => this.setState({ editDetail: text })}
                                placeholder={"Enter your name..."}
                            />
                        </View>

                        <TouchableOpacity
                            activeOpacity = {.7}
                            style = {styles.button}
                            onPress={() => this.putDataProduct(this.state.id, this.state.editName, this.state.editDetail)}
                        >
                            <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>
                                Save
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            activeOpacity = {.7}
                            style = {styles.buttonCancel}
                            onPress={() => this.setState({
                                visibleEdit: false
                            })}
                        >
                            <Text style={{textAlign: 'center', fontSize: 15}}>
                                Cancel
                            </Text>
                        </TouchableOpacity>
                    </View>
                </Modal>

                <Modal isVisible={this.state.visibleAdd} 
                    style={styles.modal}
                    onBackButtonPress={() => this.setState({ visibleAdd: false })}
                >
                    <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'flex-start' }}>
                            <Text style={{ fontSize: 30, fontWeight: 'bold', marginRight: 5}}>Add</Text>
                            <Icon 
                                name='addfile' 
                                size={20} 
                                style={{justifyContent: 'center', alignSelf: 'center'}}
                            />
                        </View>

                        <View style={[styles.frame, { marginTop: 20 }]}>
                            <Text style={styles.label}>
                                Name
                            </Text>
                            <TextInput
                                ref={(input) => { this.editName = input; }}
                                keyboardType={'default'}
                                onSubmitEditing={() => { this.editDetail.focus(); }}
                                editable={true}
                                style={styles.textInput}
                                multiline={false}
                                returnKeyType={'next'}
                                value={this.state.editName}
                                onChangeText={(text) => this.setState({ editName: text })}
                                placeholder={"Enter name product..."}
                            />
                        </View>

                        <View style={styles.frame}>
                            <Text style={styles.label}>
                                Detail
                            </Text>
                            <TextInput
                                ref={(input) => { this.editDetail = input; }}
                                keyboardType={'default'}
                                editable={true}
                                style={styles.textInput}
                                multiline={false}
                                returnKeyType={'done'}
                                value={this.state.editDetail}
                                onChangeText={(text) => this.setState({ editDetail: text })}
                                placeholder={"Enter details product..."}
                            />
                        </View>

                        <TouchableOpacity
                            activeOpacity = {.7}
                            style = {styles.button}
                            onPress={() => this.addDataProduct()}
                        >
                            <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>
                                Save
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            activeOpacity = {.7}
                            style = {styles.buttonCancel}
                            onPress={() => this.setState({
                                visibleAdd: false
                            })}
                        >
                            <Text style={{textAlign: 'center', fontSize: 15}}>
                                Cancel
                            </Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                
                <View style={{justifyContent: 'center', alignSelf: 'center', marginTop: 20}}>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>List of Products</Text>
                </View>

                <FlatList
                    style= {{ marginTop: 20 }}
                    data= {this.state.dataProducts == [] ? this.state.backUpListProduct : this.state.dataProducts}
                    // extraData={this.state}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({ item, index }) => {
                        return(
                            <View style={styles.frameList}>
                                <View style={{ flex: 0.5 }}>
                                    <Image source={Images.nmw} style={{ height: 40, width: 70 }} />
                                </View>

                                <View style={{flex: 1.5, marginLeft: 25}}>
                                    <Text>{item.name}</Text>
                                    <Text>{item.detail}</Text>
                                </View>

                                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignSelf:'center'}}>
                                    <Icon 
                                        name='delete' 
                                        size={20} 
                                        style={{width: 50, textAlign: 'center'}} 
                                        color='red' 
                                        onPress={() => this.deletePressed(item.id)}
                                    />
                                    <Icon 
                                        name='edit' 
                                        style={{width: 50, textAlign: 'center'}} 
                                        size={20} 
                                        onPress={() => this.setState({ 
                                            visibleEdit: true,
                                            id: item.id,
                                            editName: item.name,
                                            editDetail: item.detail
                                        })}
                                    />
                                </View>
                            </View>
                        )
                    }}
                />

                <TouchableOpacity style={styles.addButton} onPress={() => this.setState({visibleAdd: true})}>
                    <Text style={styles.addButtonText}>+</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    frameList: {
        flexDirection: 'row', 
        justifyContent:'center', 
        alignItems: 'center', 
        marginHorizontal: 25, 
        borderWidth: 1, 
        borderColor: '#D27025', 
        padding: 10, 
        borderRadius: 10
    },
    modal: {
        width: '80%', 
        // height: '60%', 
        backgroundColor: 'white', 
        justifyContent: 'center', 
        alignSelf: 'center',
        borderRadius: 15,
        padding: 20
    },
    frame: {
        width: '100%',
        marginTop: 10
    },
    label: {
        fontSize: 15,
        marginBottom: 5
    },
    textInput: {
        width: '100%',
        fontSize: 14,
        fontStyle: 'normal',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#D27025',
        alignSelf: 'center'
    },
    button: {
        marginTop: 20,
        width: '100%',
        padding: 10,
        backgroundColor: '#D27025', 
        borderWidth: 1, 
        borderColor: 'white',
        borderRadius: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    buttonCancel: {
        marginTop: 20,
        width: '100%',
        padding: 10,
        borderWidth: 1, 
        borderColor: '#D27025',
        borderRadius: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    addButton: {
        position: 'absolute',
        zIndex: 11,
        right: 20,
        bottom: 50,
        backgroundColor: '#D27025',
        width: 70,
        height: 70,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
    },
    addButtonText: {
        color: '#fff',
        fontSize: 24,
    },
})