import React, { Component } from 'react'
import { BackHandler, Text, View, TouchableOpacity, StyleSheet, TextInput, Image, Alert } from 'react-native'
import Images from '../Assets/Images'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            emailUser: '',
            password: ''
        }
    }

    backPressed = () => {
		Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            }, ], {
                cancelable: false
            }
        )
		return true;
    };

    async reqLogin() {
        const options = {
            headers: {
                Accept : 'application/json',
                'Content-Type' : 'application/json'
            }
        };

        let postData = JSON.stringify({
            email: this.state.emailUser,
            password: this.state.password
        });

        axios.post(`https://api.nmwclinic.co.id/login`, postData, options)
        .then((res) => {
            console.log(res.data.data)
            if(res.data.status) {
                AsyncStorage.setItem("userLogin", JSON.stringify(res.data.data))
                this.props.navigation.replace('HomeNavigation')
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.backPressed);
	}
	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    render() {
        return(
            <View style={styles.container}>
                <Image source={Images.nmw} style={{width: 200, height: 125, marginBottom: 30}} />
                <View style={styles.frame}>
                    <Text style={styles.label}>
                        Email
                    </Text>
                    <TextInput
                        ref={(input) => { this.emailUser = input; }}
                        keyboardType={'email-address'}
                        onSubmitEditing={() => { this.password.focus(); }}
                        editable={true}
                        style={styles.textInput}
                        multiline={false}
                        returnKeyType={'next'}
                        value={this.state.emailUser}
                        onChangeText={(text) => this.setState({ emailUser: text })}
                        placeholder={"Email..."}
                    />
                </View>

                <View style={[styles.frame, {marginTop: 10}]}>
                    <Text style={styles.label}>
                        Password
                    </Text>
                    <TextInput
                        ref={(input) => { this.password = input; }}
                        secureTextEntry={true}
                        editable={true}
                        style={styles.textInput}
                        multiline={false}
                        returnKeyType={'done'}
                        value={this.state.password}
                        onChangeText={(text) => this.setState({ password: text })}
                        placeholder={"Password..."}
                    />
                </View>

                <TouchableOpacity
                    activeOpacity = {.7}
                    style = {styles.button}
                    onPress={() => this.reqLogin()}
                >
                    <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>
                        Login
                    </Text>
                </TouchableOpacity>

                <View style={styles.footer}>
                    <Text>Not registered? </Text>
                    <Text style={[styles.labelFooter, {fontWeight: 'bold'}]} onPress={() => this.props.navigation.navigate('Register')}>
                        Sign up now
                    </Text>
                </View>
            </View>
        )
    }
}

export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingHorizontal: 30
    },
    frame: {
        width: '100%',
    },
    label: {
        fontSize: 15,
        marginBottom: 5
    },
    textInput: {
        width: '100%',
        fontSize: 14,
        fontStyle: 'normal',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#D27025',
        alignSelf: 'center'
    },
    button: {
        marginTop: 20,
        width: '100%',
        padding: 10,
        backgroundColor: '#D27025', 
        borderWidth: 1, 
        borderColor: 'white',
        borderRadius: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    footer: {
        flexDirection: 'row',
        marginTop: 20
    },
    labelFooter: {
        fontSize: 15,
        color: '#81BDF5'
    }
})