import React, { Component } from 'react'
import { BackHandler, Text, View, TouchableOpacity, StyleSheet, TextInput } from 'react-native'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage';

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nama: '',
            emailUser: '',
            password: '',
            rePassword: '',
            phone: '',
            photo: ''
        }
    }

    backPressed = () => {
		this.props.navigation.goBack();
		return true;
    };

    componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.backPressed);
	}
	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    reqRegister() {
        const options = {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        };

        var postData = JSON.stringify({
            email: this.state.emailUser,
            password: this.state.password,
            repassword: this.state.rePassword,
            phone: '+62'+this.state.phone,
            name: this.state.nama,
            photo: 'https://ui-avatars.com/api/?name=Muhammad+Haaris@background=0D8ABC&color=fff'
        });
        console.log(postData)

        axios.post(`https://api.nmwclinic.co.id/register`, postData, options)
        .then((res) => {
            console.log(res.data.status + ' => ' + JSON.stringify(res.data.data.firebase))
            if(res.data.status) {
                this.checkDataUser(JSON.stringify(res.data.data.firebase))

                setTimeout(() => {
                    this.props.navigation.replace('Login')
                }, 1000);
            }
        })
    }

    checkDataUser = async (item) => {
        let dataUser = await AsyncStorage.getItem('dataUser')
        let parseData = JSON.parse(dataUser)
        console.log(JSON.parse(item).displayName + ' => ' + JSON.stringify(parseData))

        if(parseData != null) {
            const dataUser = {
                nama: JSON.parse(item).displayName,
                photoUri: JSON.parse(item).photoUrl
            }

            parseData.push(dataUser)
            this.setDataUser("dataUser", JSON.stringify(parseData))
        } else {
            const dataUser = [
                {
                    nama: JSON.parse(item).displayName,
                    photoUri: JSON.parse(item).photoUrl
                }
            ]

            this.setDataUser("dataUser", JSON.stringify(dataUser))
        }
    }

    setDataUser = async (key, item) => {
        try{
            AsyncStorage.setItem(key, item)
            console.log("Data baru yang dibuat => " + await AsyncStorage.getItem(key))
        } catch(err) {
            console.log("Something error => " + err)
        }
    }

    render() {
        return(
            <View style={styles.container}>
                <Text style={styles.labelRegister}>REGISTER FORM</Text>
                <View style={styles.frame}>
                    <Text style={styles.label}>
                        Name
                    </Text>
                    <TextInput
                        ref={(input) => { this.nama = input; }}
                        keyboardType={'default'}
                        onSubmitEditing={() => { this.emailUser.focus(); }}
                        editable={true}
                        style={styles.textInput}
                        multiline={false}
                        returnKeyType={'next'}
                        value={this.state.nama}
                        onChangeText={(text) => this.setState({ nama: text })}
                        placeholder={"Enter your name..."}
                    />
                </View>

                <View style={[styles.frame, {marginTop: 10}]}>
                    <Text style={styles.label}>
                        Email
                    </Text>
                    <TextInput
                        ref={(input) => { this.emailUser = input; }}
                        keyboardType={'email-address'}
                        onSubmitEditing={() => { this.password.focus(); }}
                        editable={true}
                        style={styles.textInput}
                        multiline={false}
                        returnKeyType={'next'}
                        value={this.state.emailUser}
                        onChangeText={(text) => this.setState({ emailUser: text })}
                        placeholder={"example@example.com"}
                    />
                </View>

                <View style={[styles.frame, {marginTop: 10}]}>
                    <Text style={styles.label}>
                        Password
                    </Text>
                    <TextInput
                        ref={(input) => { this.password = input; }}
                        onSubmitEditing={() => { this.rePassword.focus(); }}
                        secureTextEntry={true}
                        editable={true}
                        style={styles.textInput}
                        multiline={false}
                        returnKeyType={'next'}
                        value={this.state.password}
                        onChangeText={(text) => this.setState({ password: text })}
                        placeholder={"Min 6 digits..."}
                    />
                </View>

                <View style={[styles.frame, {marginTop: 10}]}>
                    <Text style={styles.label}>
                        Re-password
                    </Text>
                    <TextInput
                        onSubmitEditing={() => { this.phone.focus(); }}
                        ref={(input) => { this.rePassword = input; }}
                        secureTextEntry={true}
                        editable={true}
                        style={styles.textInput}
                        multiline={false}
                        returnKeyType={'next'}
                        value={this.state.rePassword}
                        onChangeText={(text) => this.setState({ rePassword: text })}
                        placeholder={"Min 6 digits..."}
                    />
                </View>

                <View style={[styles.frame, {marginTop: 10}]}>
                    <Text style={styles.label}>
                        Phone
                    </Text>
                    <TextInput
                        ref={(input) => { this.phone = input; }}
                        keyboardType={'phone-pad'}
                        editable={true}
                        style={styles.textInput}
                        multiline={false}
                        returnKeyType={'done'}
                        value={this.state.phone}
                        onChangeText={(text) => this.setState({ phone: text.replace(/[^0-9]/g, '') })}
                        placeholder={"example : 89923XXXX"}
                    />
                </View>

                <TouchableOpacity
                    activeOpacity = {.7}
                    style = {styles.button}
                    onPress={() => this.reqRegister()}
                >
                    <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>
                        REGISTER
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default Register;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingHorizontal: 30
    },
    labelRegister: {
        fontSize: 25, 
        marginBottom: 15, 
        fontWeight: 'bold'
    },
    frame: {
        width: '100%',
    },
    label: {
        fontSize: 15,
        marginBottom: 5
    },
    textInput: {
        width: '100%',
        fontSize: 14,
        fontStyle: 'normal',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#D27025',
        alignSelf: 'center'
    },
    button: {
        marginTop: 20,
        width: '100%',
        padding: 10,
        backgroundColor: '#D27025', 
        borderWidth: 1, 
        borderColor: 'white',
        borderRadius: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    footer: {
        flexDirection: 'row',
        marginTop: 10
    },
    labelFooter: {
        fontSize: 15,
        color: '#81BDF5'
    }
})